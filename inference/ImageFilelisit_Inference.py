import torch.utils.data as data

from PIL import Image
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
import os
import os.path

def default_loader(path):
	return Image.open(path).convert('RGB')

def default_flist_reader(flist):
	"""
	flist format: impath label\nimpath label\n ...(same to caffe's filelist)
	"""
	imlist = []
	with open(flist, 'r') as rf:     
		for line in rf.readlines():
			impath = line.strip().split()[0]
			imlist.append( (impath) )
	return imlist

class ImageFilelisit_a():
	def __init__(self, flist, transform=None, target_transform=None,
			flist_reader=default_flist_reader, loader=default_loader):
		#self.root   = root
		self.imlist = flist_reader(flist)
		self.transform = transform
		self.target_transform = target_transform
		self.loader = loader

	def __getitem__(self, index):
		impath = self.imlist[index]
		#img = self.loader(os.path.join(self.root,impath))
		img = self.loader(impath)
		if self.transform is not None:
			img = self.transform(img)
		return img, impath

	def __len__(self):
		return len(self.imlist)
'''
	if __name__ == '__main__':
		ImageFilelist = ImageFilelist()
		#return ImageFilelist
''' 
