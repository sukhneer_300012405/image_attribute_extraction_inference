import pandas as pd
import os
import json
import operator
import configparser 
from Evaluation_Utils import evaluation_utils



class ImageAttributeInference():
    def __init__(self):
        self.config = self.read_config()
        self.utils = evaluation_utils()
        self.model_ft=self.load_models()
        
    
    def read_config(self):  
        configParser = configparser.ConfigParser()   
        configFilePath = 'config_inference.cfg'
        configParser.read(configFilePath)
        config = {}
        for section in configParser.sections():
            config[section] = {}
            for options in configParser.options(section):
                config[section][options] = configParser.get(section, options)
        return config
            
    
    def load_models(self):
        all_articles = {k: json.loads(v.replace("'", '"')) for k,v in self.config['attributes'].items()}
        self.all_articles = all_articles
        base_path = self.config['model_details']['base_path']
        n_classes_dict_path_folder = self.config['model_details']['n_classes_dict_path_folder']
        n_classes_dict_suffix= self.config['model_details']['n_classes_dict_suffix']
        ckpt_parent_folder = self.config['model_details']['ckpt_parent_folder']
        architecture_folder_name = self.config['model_details']['architecture_folder_name']
        self.reverse_label_map_dict = {}
        model_ft = {}
        print(base_path, ckpt_parent_folder,
                                               architecture_folder_name)
        for article in all_articles:
            print("working for article : "+article)
            n_classes_dict_path = os.path.join(base_path, article, n_classes_dict_path_folder,
                                               article+n_classes_dict_suffix)
            model_path = os.path.join(base_path, article, ckpt_parent_folder,
                                               architecture_folder_name)
            label_map_dict={}
            for attrib in all_articles[article]:
                label_map_dict[attrib] = os.path.join(base_path,article, article + '_' + attrib,
                                                      article + '_' + attrib + '_label_map.txt')
            with open(n_classes_dict_path) as json_data:
                        n_classes_dict = json.load(json_data)
             

            self.reverse_label_map_dict[article] = self.utils.get_reverse_label_map_dict(label_map_dict,all_articles,article)
            print("reverse label map : "+str( self.reverse_label_map_dict))
            print("model path to pick model : "+model_path)
            if os.path.exists(model_path):
                model_ft[article]= self.utils.load_model(n_classes_dict, all_articles[article], model_path)  # attributes, modelPath are defined in config file
            else:
                print("model for " + article + " not found! ")

        return model_ft
    
  
    def get_attributes(self,input_json):
            article = input_json['article']
            
            if input_json['type']=="url":
                path_to_image = self.utils.download_image(input_json['path'], self.config['img_store']['image_store_path'])
                print("path to image is :"+path_to_image)
                results = self.utils.get_single_image_prediction(self.model_ft[article], path_to_image, self.all_articles[article], self.reverse_label_map_dict[article])
                print (results)

            else:
                dataloaders = self.utils.get_data_loaders(path_to_input_file)      # path_to_input_file is defined in config file
                results = self.utils.get_batch_prediction(self.model_ft, dataloaders, attributes, reverse_label_map_dict)
            #     accuracy_attr_wise, overall_accuracy = eval_obj.get_accuracy(results, attributes)

            return results

'''
{
  "article": "dresses",
  "type": "url",
  "path" : "url or file path"
}
'''
   
if __name__ == "__main__":
    ImageAttributeInference_obj = ImageAttributeInference()
    json = {
              "article": "dresses",
              "type": "url",
              "path" : "https://assets.myntassets.com/h_1440,q_100,w_1080/v1/image/style/properties/896009/Miss-Chase-Black-Skater-Dress_1_d5d5dd9802ac8fcc175ea9dabfe22562.jpg"
            }
    ImageAttributeInference_obj.get_attributes(json)
    
    