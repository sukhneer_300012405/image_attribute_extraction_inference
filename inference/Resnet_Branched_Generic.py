import os
os.environ["CUDA_VISIBLE_DEVICES"]="0" 
import torch.nn as nn
from torchvision import models
import copy
from torchvision.models.resnet import BasicBlock

class Resnet_branched(models.ResNet):
    def __init__(self, n_classes_dict, attributes, block = BasicBlock, layers = [3, 4, 6, 3], num_classes=1000):
        super(Resnet_branched, self).__init__(block, layers, num_classes)
        self.attributes = attributes

        model_ft = models.resnet34(pretrained=True)
        model_state_dict = model_ft.state_dict()
        self.load_state_dict(model_state_dict)
        
        del self.avgpool, self.fc
        self.attr = nn.ModuleList()
        for attrib in attributes:
            self.attr.append(copy.deepcopy(self.layer4))
        
        del self.layer4
        self.avgpool = nn.AvgPool2d(7, stride=1)
        
        self.linear = nn.ModuleList()
        for attrib in attributes:
            self.linear.append(nn.Linear(512, n_classes_dict[attrib]))
            
    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        
        return_vars = []
#         print ('\n\nattributes of self:' , dir(self) )
        for i, attrib in enumerate(self.attributes):
            y = self.attr[i](x)
            y = self.avgpool(y)
            y = y.view(y.size(0), -1)
            y = self.linear[i](y)
            return_vars.append(y.cuda())  
        return return_vars