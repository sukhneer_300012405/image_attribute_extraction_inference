from __future__ import print_function, division

import os
import sys
os.environ["CUDA_VISIBLE_DEVICES"]="0" 
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import torchvision
from torchvision import datasets, models, transforms
import time, json, glob
import pandas as pd
import numpy as np
from datetime import datetime
from tqdm import tqdm
from PIL import Image
import requests

sys.path.insert(0,'../')
import ImageFilelisit_Inference as ImageFileList
from Resnet_Branched_Generic import *
#from config import *

class evaluation_utils():
    def get_data_loaders(self, path_to_input_file):

        data_transforms = {
            'val': transforms.Compose([
                transforms.Resize( (224,224) ),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ])
        }

        image_datasets = {x: ImageFileList.ImageFilelisit_a( flist=path_to_input_file[x],
                                           transform = data_transforms[x]) for x in ['val']}

        dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size= batch_size_num,
                                                     shuffle=False, num_workers=4) for x in ['val']}

        dataset_sizes = len(image_datasets['val'])
        print ('dataset_sizes: ' + str(dataset_sizes) )

        return dataloaders

    def download_image(self,url,image_store_path):
        fname = url.split('/')[-1]
        fname = os.path.join(image_store_path,fname)
        if not os.path.exists(image_store_path):
            os.makedirs(image_store_path)
        path_to_image = ""
        try:
            r = requests.get(url)
            with open(fname, "wb") as f:
                f.write(r.content)
                path_to_image = fname
        except Exception as e:
                print(e)
        return path_to_image


    # fn to load model file
    def load_model(self, n_classes_dict,attributes, modelPath):

        model_ft = Resnet_branched(n_classes_dict, attributes)
        list_of_files = glob.glob(modelPath+"/*") 
        model_file = max(list_of_files, key=os.path.getctime)
        print("picking model file : "+str(model_file))

        checkpoint = torch.load(model_file)
        epoch = checkpoint['epoch']
        best_acc = checkpoint['best_acc']
        model_ft.load_state_dict(checkpoint['state_dict'])

        if torch.cuda.is_available():
            model_ft = model_ft.cuda()
        model_ft.eval()
        return model_ft


    def get_reverse_label_map_dict(self, label_map_dict,all_articles,article):

        reverse_label_map_dict = {}
        for attr in all_articles[article]:
            with open(label_map_dict[attr]) as json_data:
                label_map = json.load(json_data)  

            temp = {j:i for i,j in label_map.items()}  # number to name
            reverse_label_map_dict[attr] = temp
        return reverse_label_map_dict

    # function to predict
    def get_single_image_prediction(self, model_ft, path_to_image, attributes, reverse_label_map_dict):

        loader = transforms.Compose([
                    transforms.Resize( (224,224) ),
                    transforms.ToTensor(),
                    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]) ])

        image = Image.open(path_to_image)
        image = loader(image).float()
        image = Variable(image, requires_grad=True)
        image = image.unsqueeze(0)

        if torch.cuda.is_available():
            image = image.cuda()

        outputs = model_ft(image)
        result_batch = pd.DataFrame()
        
        for i, attrib in enumerate(attributes):
            _, pred = torch.max(outputs[i].data, 1)
            attrib_dict = reverse_label_map_dict[attrib]
            pred_name = attrib_dict[int(pred[0])] 
            prob = np.max (self.softmax (outputs[i].data[0].cpu().numpy() ) )
            temp = pd.DataFrame({ 'Predicted_'  + attrib : [pred_name], 'Prob_' + attrib : [prob] })
            result_batch = pd.concat([result_batch, temp], axis =1)
        result_batch['image_url'] = path_to_image
        return result_batch


    def get_batch_prediction(self, model_ft, dataloaders, attributes, reverse_label_map_dict):

        results_val = pd.DataFrame()
        for i, data in tqdm(enumerate(dataloaders['val'])):
            inputs, impath = data
            inputs = Variable(inputs)
            if torch.cuda.is_available():
                inputs = inputs.cuda() 

            outputs = model_ft(inputs)
            preds = {}
            result_batch = pd.DataFrame()
            for i, attr in enumerate(attributes):

                _, pred = torch.max(outputs[i].data, 1) 
                result_attr = pd.DataFrame()
                for j in range(outputs[i].size()[0]):
                    attrib_dict = reverse_label_map_dict[attr]
                    pred_name = attrib_dict[int(pred[j])] 
                    prob = np.max (self.softmax (outputs[i].data[j].cpu().numpy() ) )
                    temp = pd.DataFrame({'Predicted_'  + attr : [pred_name], 'Prob_' + attr : [prob] })
                    result_attr = result_attr.append(temp)
                
                result_batch = pd.concat([result_batch, result_attr], axis =1)
                result_batch['path'] = impath
                

            results_val = results_val.append(result_batch).reset_index(drop = True)
            results_val = results_val[ ['path'] + [x for x in results_val.columns if x != 'path'] ]

        return results_val


    def softmax(self, x):
        """Compute softmax values for each sets of scores in x."""
        e_x = np.exp(x - np.max(x))
        return e_x / e_x.sum()
